<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 't_brand';
    protected $fillable = [
        'brand_name'
    ];

    public function camera()
    {
        return $this->hasMany('App\Camera', 'brand_id');
    }
}
