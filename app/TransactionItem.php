<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    protected $table = 't_transaction_item';
    protected $fillable = [
        'transaction_id',
        'camera_id',
        'item_qty',
        'item_price',
        'item_subtotal'
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction', 'transaction_id');
    }

    public function camera()
    {
        return $this->belongsTo('App\Camera', 'camera_id');
    }
}
