<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camera extends Model
{
    protected $table = 't_camera';
    protected $fillable = [
        'brand_id',
        'camera_name',
        'camera_price'
    ];

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id');
    }

    public function transactionItem()
    {
        return $this->hasMany('App\TransactionItem', 'camera_id');
    }
}
