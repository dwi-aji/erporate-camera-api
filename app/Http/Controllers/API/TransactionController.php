<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Camera;
use App\Transaction;
use App\TransactionItem;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaction = Transaction::all();
        return response()->json([
            'status' => true,
            'message' => 'Data Fetched',
            'data' => $transaction
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => ['required'],
            'camera_id.*' => ['required'],
            'item_qty.*' => ['required', 'numeric', 'min:1'],
        ]);

        $total = 0;
        $transactionArr = array();
        foreach($request->camera_id as $key => $value){
            $camera = Camera::findOrFail($value);
            $qty = $request->item_qty[$key];
            $sub_total = $camera->camera_price * $qty;
            $total += $sub_total;

            $transactionArr[] = new TransactionItem([
                'camera_id' => $camera->id,
                'item_qty' => $qty,
                'item_price' => $camera->camera_price,
                'item_subtotal' => $sub_total
            ]);
        }

        $transaction = new Transaction;
        $transaction->user_id = $request->user_id;
        $transaction->transaction_status = 'pending';
        $transaction->transaction_total = $total;
        $transaction->save();

        $transaction_item = $transaction->transactionItem()->saveMany($transactionArr);

        return response()->json([
            'status' => true,
            'message' => 'Data Stored',
            'data' => $transaction,
            'transaction_item' => $transaction_item
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::findOrFail($id);
        return response()->json([
            'status' => true,
            'message' => 'Data Fetched',
            'data' => $transaction,
            'transaction_item' => $transaction->transactionItem
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);
        
        $request->validate([
            'transaction_status' => ['required', 'string']
        ]);

        $transaction->transaction_status = $request->transaction_status;
        $transaction->save();

        return response()->json([
            'status' => true,
            'message' => 'Data Updated',
            'data' => $transaction,
            'transaction_item' => $transaction->transactionItem
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::findOrFail($id);
        $transaction->delete();

        return response()->json([
            'status' => true,
            'message' => 'Data Deleted',
        ]);
    }
}
