<?php

namespace App\Http\Controllers\API;

use App\Camera;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CameraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $camera = Camera::with('brand')->get();
        return response()->json([
            'status' => true,
            'message' => 'Data Fetched',
            'data' => $camera
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'brand_id' => ['required'],
            'camera_name' => ['required', 'string', 'unique:t_camera,camera_name'],
            'camera_price' => ['required', 'numeric', 'min:0']
        ]);

        $camera = new Camera;
        $camera->brand_id = $request->brand_id;
        $camera->camera_name = $request->camera_name;
        $camera->camera_price = $request->camera_price;
        $camera->save();

        return response()->json([
            'status' => true,
            'message' => 'Data Stored',
            'data' => $camera,
            'brand' => $camera->brand
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $camera = Camera::findOrFail($id);
        return response()->json([
            'status' => true,
            'message' => 'Data Fetched',
            'data' => $camera,
            'brand' => $camera->brand
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $camera = Camera::findOrFail($id);
        $request->validate([
            'brand_id' => ['required'],
            'camera_name' => ['required', 'string', 'unique:t_camera,camera_name,'.$id],
            'camera_price' => ['required', 'numeric', 'min:0']
        ]);

        $camera->brand_id = $request->brand_id;
        $camera->camera_name = $request->camera_name;
        $camera->camera_price = $request->camera_price;
        $camera->save();

        return response()->json([
            'status' => true,
            'message' => 'Data Updated',
            'data' => $camera,
            'brand' => $camera->brand
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Camera  $camera
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $camera = Camera::findOrFail($id);
        $camera->delete();

        return response()->json([
            'status' => true,
            'message' => 'Data Deleted',
        ]);
    }
}
