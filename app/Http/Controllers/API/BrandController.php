<?php

namespace App\Http\Controllers\API;

use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::all();
        return response()->json([
            'status' => true,
            'message' => 'Data Fetched',
            'data' => $brand
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'brand_name' => ['required', 'string', 'unique:t_brand,brand_name']
        ]);

        $brand = new Brand;
        $brand->brand_name = $request->brand_name;
        $brand->save();

        return response()->json([
            'status' => true,
            'message' => 'Data Stored',
            'data' => $brand
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::findOrFail($id);
        return response()->json([
            'status' => true,
            'message' => 'Data Fetched',
            'data' => $brand,
            'camera' => $brand->camera
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'brand_name' => ['required', 'string', 'unique:t_brand,brand_name,'.$id]
        ]);

        $brand = Brand::findOrFail($id);
        $brand->brand_name = $request->brand_name;
        $brand->save();

        return response()->json([
            'status' => true,
            'message' => 'Data Update',
            'data' => $brand
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        $brand->delete();

        return response()->json([
            'status' => true,
            'message' => 'Data Deleted',
        ]);
    }
}
