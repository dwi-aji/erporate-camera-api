<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v0'
], function(){
    Route::post('register', 'API\UserController@register');
    Route::post('/login', 'API\UserController@login');

    Route::group([
        'middleware' => 'auth:api'
    ], function(){
        Route::get('/user', 'API\UserController@detail');
        Route::post('/logout', 'API\UserController@logout');

        // Brand
        Route::get('/brand', 'API\BrandController@index');
        Route::get('/brand/{id}', 'API\BrandController@show');
        Route::post('/brand', 'API\BrandController@store');
        Route::put('/brand/{id}', 'API\BrandController@update');
        Route::delete('/brand/{id}', 'API\BrandController@destroy');

        // Camera
        Route::get('/camera', 'API\CameraController@index');
        Route::get('/camera/{id}', 'API\CameraController@show');
        Route::post('/camera', 'API\CameraController@store');
        Route::put('/camera/{id}', 'API\CameraController@update');
        Route::delete('/camera/{id}', 'API\CameraController@destroy');

        // Transaction
        Route::get('/transaction', 'API\TransactionController@index');
        Route::get('/transaction/{id}', 'API\TransactionController@show');
        Route::post('/transaction', 'API\TransactionController@store');
        Route::put('/transaction/{id}', 'API\TransactionController@update');
        Route::delete('/transaction/{id}', 'API\TransactionController@destroy');
    });
});