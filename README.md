<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Getting Started
- Clone this project
```
git clone https://gitlab.com/dwi-aji/erporate-camera-api.git
```
- Copy .env.example to .env
```
# Windows
copy .env.example .env

# Linux
cp .env.example .env
```
- Run composer install
```
composer install
```
- Make Laravel APP_KEY
```
php artisan key:generate
```
- Config your connection (.env)
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
- Run migration
```
php artisan migrate
```
- Install Passport
```
php artisan passport:install
```
- Run server
```
php artisan serve
```

## POSTMAN Documentation
[Postman Documentation](https://documenter.getpostman.com/view/7495597/SW7gU5Bu?version=latest)