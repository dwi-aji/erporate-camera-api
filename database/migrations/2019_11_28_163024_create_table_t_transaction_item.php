<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTTransactionItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_transaction_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('transaction_id')->unsigned();
            $table->bigInteger('camera_id')->unsigned();
            $table->integer('item_qty');
            $table->decimal('item_price', 20, 2);
            $table->decimal('item_subtotal', 20, 2);
            $table->timestamps();

            $table->foreign('transaction_id')
                ->references('id')
                ->on('t_transaction')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('camera_id')
                ->references('id')
                ->on('t_camera')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_transaction_item');
    }
}
